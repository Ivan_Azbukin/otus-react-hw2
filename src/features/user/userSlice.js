import { createSlice } from '@reduxjs/toolkit'

export const userSlice = createSlice({
    name: 'user',
    initialState:{
        email: ""
    },
    reducers:{
        signInUser: (state, action) =>{
            state.email = action.payload;
        }

    }
});

export const {signInUser} = userSlice.actions;

export const selectEmail = state => state.user.email;

export default userSlice.reducer;