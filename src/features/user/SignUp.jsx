import {Form, Button, Jumbotron, Container } from 'react-bootstrap';

import {useHistory } from "react-router-dom";
import React, { useState } from 'react';
import {useDispatch } from 'react-redux';
import {signInUser} from "./userSlice";

const SignUp = () =>{

    const dispatch = useDispatch();

    const [inputEmail, setInputEmail] = useState("");

    let history = useHistory();

    const submitForm = () =>{
        history.push("/");
    }

    return (
         <Container className="app">
            <Jumbotron className="vertical-center">
                <h1>Sign Up</h1>
                <Form onSubmit={() => submitForm()}>
                    <Form.Group controlId="formBasicEmail" >
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email"
                            placeholder="Enter email"
                            value={inputEmail}
                            onChange={e => setInputEmail(e.target.value)}    
                        />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Confirm password</Form.Label>
                        <Form.Control type="password" placeholder="Confirm password" />
                    </Form.Group>

                    <Button variant="dark" type="submit" onClick={() => dispatch(signInUser(inputEmail))}>
                        Sign Up
                    </Button>
                </Form>
            </Jumbotron>
        </Container>
    );
};

export default SignUp;