import { Jumbotron } from 'react-bootstrap';

import React from 'react';
import { useSelector} from 'react-redux';
import {selectEmail} from "../features/user/userSlice";

const HomePage = () =>{

    const email = useSelector(selectEmail);

    return(
        <Jumbotron>
            <h1>{`Hello, ${email}!`}</h1>
            <p> This is home page</p>
        </Jumbotron>
    );
};


export default HomePage;