import { Jumbotron } from 'react-bootstrap';

const AppNotFound = () =>{
    return (
        <Jumbotron>
            <h1>Page not found :c</h1>
        </Jumbotron>
    );

};

export default AppNotFound;