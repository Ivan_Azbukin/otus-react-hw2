import React from 'react';

import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import { Navbar, Nav} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap'

import HomePage from './Components/HomePage';
import AppNotFound from './Components/AppNotFound';
import SignIn from './features/user/SignIn';
import SignUp from './features/user/SignUp';

import {Helmet} from "react-helmet";

function App() {
  return (
    <Router>
      <Helmet>
          <meta charSet="utf-8" />
          <title>My HW</title>
      </Helmet>

      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Navbar.Brand>My HW</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <LinkContainer to="/">
              <Nav.Link>Home</Nav.Link>
            </LinkContainer>
          </Nav>
          <Nav>

            <LinkContainer to="/SignIn">
              <Nav.Link>SignIn</Nav.Link>
            </LinkContainer>

            <LinkContainer to="/SignUp">
              <Nav.Link>SignUp</Nav.Link>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      <Switch>
        <Route exact path = "/">
          <HomePage/>
        </Route>

        <Route path = "/SignUp">
          <SignUp/>
        </Route>

        <Route path = "/SignIn">
          <SignIn/>
        </Route>

        <Route path = "*">
          <AppNotFound/>
        </Route>
      </Switch>
    </Router>
   
  );
}

export default App;
